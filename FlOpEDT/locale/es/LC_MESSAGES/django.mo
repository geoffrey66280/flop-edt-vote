��    E      D  a   l      �  	   �  	   �          $     8     >  	   R     \     c     q     �     �     �     �     �     �     �     �     �     �     �     �     �  
   �          
               $     )     5     G     T  	   g     q     �     �     �  	   �     �     �     �     �     �     �               )     1     :     B  
   P     [  J   `     �     �     �     �     �     �     	     		     	     $	  	   0	     :	  
   ?	     J	  B  O	     �
     �
  "   �
     �
     �
     �
  
              &     6     P     W     ]     t     |     �     �     �     �     �     �     �     �     �     �     �     �     �     �               #     8     K     Q     m  ,   z     �     �     �     �     �     �     �     �     �      �          .     =     I     ^     m  G   r     �     �     �  '   �  #   �           %     -     6     >     F     S     X     h     #   (             <         "   ,                     D   4                  6           =   1       >   8             $   &      A       ?          @   +   E   )       :              5              /       -   7   3      B             %         0              
       C   *       ;      !       .                    '         	         2       9           A student A teacher A tech or administrative staff A temporary teacher Admin Author description  Category  Create Create a link Create or modify a link Created Date  Default week English First name  Forgot password? French Fri. Generate Header  Help I am Import Last name  Maximum Messages Modified Modules Mon. Move/Cancel My favorite rooms My ideal day My videoconf links Nickname  No such user as {user} Not allowed Not authorized. New link then. Preferences Preferred Quote  Rooms Sat. Schedule_banner Schedule_title Select my favorite rooms Send Set default week Sign in Sign out Sign up Signed in as  Statistics Sun. This is the total number of teaching hours I would like to give every day: Thu. Tue. Tutor does not exist Unknown link. New link then. Update my default week Wed. Weeks button_apply button_change button_save from week iCal until week year Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Un·a estudiante Un·a profesor·a permanente Personal técnico o administrativo Un profesor temporal Admin Descripción del autor Categoría Crear Crear un enlace Creer o cambiar un enlace Creado Fecha Semanas predeterminada Inglés Nombre ¿Has olvidado tu contraseña? Francés Vie. Generar Título Ayuda Soy Importar Apelido Maximum Mensajes Cambiado Módulos Lun. Mover/Cancelar Mi aula favorita Mis días preferidos Mi enlace de clase Apodo El usuario {user} no existe No permitido No tienes autorización. Nuevo enlace creado Preferencias  Favorito Cita Aulas Sab. Horario Horario Elegir mi aula favorita Mandar Configurar semana predeterminada Iniciar sesión Cerrar sesión Inscribirse Iniciar sesión como Estadísticas  Dom. Eso es el total de horas de educación que quiero hacer todos los días Jue. Mar. Tutor no existe Enlace desconocido. Nuevo enlace creado Actualizar mi semana predeterminada Mie. Semanas Applicar Cambiar Guardar Desde semana iCal hasta la semana años 